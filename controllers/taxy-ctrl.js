const Taxy = require('../models/taxy-model')

createTaxy = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a taxy',
        })
    }

    const taxy = new Taxy(body)

    if (!taxy) {
        return res.status(400).json({ success: false, error: err })
    }

    taxy
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: taxy._id,
                message: 'taxy created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'taxy not created!',
            })
        })
}

updateTaxy = async (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    Taxy.findOne({ _id: req.params.id }, (err, taxy) => {
        if (err) {
            return res.status(404).json({
                err,
                message: 'taxy not found!',
            })
        }
        taxy.name = body.name
        taxy.time = body.time
        taxy.rating = body.rating
        taxy
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: taxy._id,
                    message: 'taxy updated!',
                })
            })
            .catch(error => {
                return res.status(404).json({
                    error,
                    message: 'taxy not updated!',
                })
            })
    })
}

deleteTaxy = async (req, res) => {
    await Taxy.findOneAndDelete({ _id: req.params.id }, (err, taxy) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!taxy) {
            return res
                .status(404)
                .json({ success: false, error: `taxy not found` })
        }

        return res.status(200).json({ success: true, data: taxy })
    }).catch(err => console.log(err))
}

getTaxyById = async (req, res) => {
    await Taxy.findOne({ _id: req.params.id }, (err, taxy) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!taxy) {
            return res
                .status(404)
                .json({ success: false, error: `taxy not found` })
        }
        return res.status(200).json({ success: true, data: taxy })
    }).catch(err => console.log(err))
}

getTaxys = async (req, res) => {
    await Taxy.find({}, (err, taxys) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!taxys.length) {
            return res
                .status(404)
                .json({ success: false, error: `taxy not found` })
        }
        return res.status(200).json({ success: true, data: taxys })
    }).catch(err => console.log(err))
}

module.exports = {
    createTaxy,
    updateTaxy,
    deleteTaxy,
    getTaxys,
    getTaxyById,
}