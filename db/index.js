const mongoose = require('mongoose')

mongoose
    .connect('mongodb://188.40.205.135:27020/taxy', { useNewUrlParser: true })
    .catch(e => {
        console.error('Connection error', e.message)
    })

const db = mongoose.connection

module.exports = db