FROM node:latest
#COPY package.json package.json
COPY . .
#RUN npm uninstall

#RUN npm cache clean --force
#RUN npm install -g npm 
#RUN npm rebuild

EXPOSE 3000
CMD ["npm","start"]
