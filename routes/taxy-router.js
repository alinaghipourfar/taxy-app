const express = require('express')

const TaxyCtrl = require('../controllers/taxy-ctrl')

const router = express.Router()

router.post('/taxy', TaxyCtrl.createTaxy)
router.put('/taxy/:id', TaxyCtrl.updateTaxy)
router.delete('/taxy/:id', TaxyCtrl.deleteTaxy)
router.get('/taxy/:id', TaxyCtrl.getTaxyById)
router.get('/taxys', TaxyCtrl.getTaxys)

module.exports = router